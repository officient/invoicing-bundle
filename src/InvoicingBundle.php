<?php

namespace Officient\Invoicing\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class InvoicingBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\Invoicing\Symfony\InvoicingBundle
 */
class InvoicingBundle extends Bundle
{
    //
}